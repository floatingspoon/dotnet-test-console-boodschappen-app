using System;
using System.Collections.Generic;

namespace Grocery_List
{

    // Source: https://www.youtube.com/watch?v=YiE0oetGMAg&list=PLPV2KyIb3jR4CtEelGPsmPzlvP7ISPYzR&index=6
    // Source: Avans+ C# programmeercursus
    // Source: https://www.tutorialsteacher.com/csharp/csharp-while-loop


    class Program
    {
        static void Main(string[] args)
        {
            // Menu

            List<string> grocerylist = new List<string>();

            int answer = 0;
            int firsttime = 0;
            bool exit_program = false;

            while (answer <= 7 && exit_program == false)
            {
                // Menu (DONE)
                Console.WriteLine("\n-----------------------------------------------------------");

                // Different greetings (DONE)
                if (firsttime <= 0)
                { 
                    Console.WriteLine("\nWelcome, what do you want to do? \n");
                    firsttime++;
                } 
                else if (firsttime >= 1) 
                { 
                    Console.WriteLine("\nWelcome back! What do you want to do? \n"); 
                }

                // Menu options (DONE)
                Console.WriteLine("\t1. Show your grocery list");
                Console.WriteLine();
                Console.WriteLine("\t2. Add an item to your grocery list");
                Console.WriteLine("\t3. Adjust an item on your grocery list");
                Console.WriteLine("\t4. Remove an item from your grocery list");
                Console.WriteLine("\t5. Clear grocery list");
                Console.WriteLine();
                Console.WriteLine("\t6. Play a song for during grocery shopping");
                Console.WriteLine("\t7. Interactive grocery list shopping (COMING SOON)");
                Console.WriteLine();
                Console.WriteLine("\t0. Close this program");

                Console.Write("\nPlease enter your choice: ");
                answer = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("\n-----------------------------------------------------------\n");


                // 1: Showing the grocery list (DONE)
                if (answer == 1)
                {
                    Console.WriteLine("\nYour grocery list: \n");
                    for (int i = 0; i < grocerylist.Count; i++)
                    {
                        Console.WriteLine("\t" + grocerylist[i]);
                    }
                    Console.Write("\nPress enter to continue: ");
                    Console.ReadLine();
                }


                // 2: Adding an item to the grocery list (DONE)
                else if (answer == 2)
                {
                    bool adding_item = true;
                    
                    while (adding_item == true)
                    {
                        Console.Write("Please enter the name of your item: ");
                        grocerylist.Add(Console.ReadLine());
                        Console.WriteLine("This item has been added to your grocery list.\n");
                        
                        // Adding another item
                        Console.Write("Do you want to add another item to your list? (y/n): ");
                        string another_item = Console.ReadLine();

                        if (another_item == "y")
                        {
                            adding_item = true;
                        }
                        else
                        {
                            adding_item = false;
                        }
                    }
                }


                // 3: Adjusting an item on your grocery list
                else if (answer == 3)
                {
                    bool adjusting_item = true;

                    while (adjusting_item == true)
                    {
                        Console.WriteLine("\nYour grocery list: \n");
                        for (int i = 0; i < grocerylist.Count; i++)
                        {
                            Console.WriteLine($"\t{i + 1}. " + grocerylist[i]);
                        }
                        Console.Write("\nWhich item do you want to replace/adjust?: ");
                        int replace_item = Convert.ToInt32(Console.ReadLine());
                        grocerylist.RemoveAt(replace_item - 1);
                        Console.Write("Adjust item to: ");
                        grocerylist.Insert(replace_item - 1, Console.ReadLine());

                        // Adding another item
                        Console.Write("Do you want to adjust another item on your list? (y/n): ");
                        string another_item = Console.ReadLine();

                        if (another_item == "y")
                        {
                            adjusting_item = true;
                        }
                        else
                        {
                            adjusting_item = false;
                        }
                    }
                }


                // 4: Removing an item from the grocery list
                else if (answer == 4)
                {
                    bool removing_item = true;

                    while (removing_item == true)
                    {
                        Console.WriteLine("\nYour grocery list: ");
                        for (int i = 0; i < grocerylist.Count; i++)
                        {
                            Console.WriteLine($"\t{i + 1}. " + grocerylist[i]);
                        }
                        Console.Write("\nWhich item do you want to remove?: ");
                        int remove_item = Convert.ToInt32(Console.ReadLine());
                        grocerylist.RemoveAt(remove_item - 1);

                        // Removing another item
                        Console.Write("Do you want to remove another item from your list? (y/n): ");
                        string another_item = Console.ReadLine();

                        if (another_item == "y")
                        {
                            removing_item = true;
                        }
                        else
                        {
                            removing_item = false;
                        }
                    }
                }


                // 5: Clear grocery list (DONE)
                else if (answer == 5)
                {
                    Console.Write("Do you want to clear your current grocery list? (y/n): ");
                    string user_clear = Console.ReadLine();
                    if (user_clear == "y")
                    {
                        grocerylist.Clear();
                        Console.WriteLine("Your grocery list has been cleared.");
                        Console.Write("Press enter to continue: ");
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.Write("Nothing has been cleared. Press enter to continue: ");
                        Console.ReadLine();
                    }
                }


                // 6: Play a song for grocery shopping
                else if (answer == 6)
                {
                    Console.WriteLine("Press enter to play: 'Keep on Movin'' by Caron Wheeler & Soul II Soul");
                    Console.ReadLine();
                    Console.WriteLine("\nArtist: Caron Wheeler & Soul II Soul");
                    Console.WriteLine("Song: Keep on Movin'");
                    Console.WriteLine("");
                    Console.WriteLine(
                        "\t" + "Keep on moving" + "\n" +
                        "\t" + "Don't stop, like the hands of time" + "\n" +
                        "\t" + "Click - clock, find your own way to stay" + "\n" +
                        "\t" + "The time will come one day" + "\n" +
                        "\t" + "\n" +
                        "\t" + "Why do people choose to live their lives (lives)" + "\n" +
                        "\t" + "\n" +
                        "\t" + "This way?" + "\n" +
                        "\t" + "\n" +
                        "\t" + "No - oh - oh" + "\n" +
                        "\t" + "(Keep on moving)" + "\n" +
                        "\t" + "(Keep on moving) Keep on moving yeah, yeah" + "\n" +
                        "\t" + "(Keep on moving, don't stop, no)" + "\n" +
                        "\t" + "(Keep on moving)" + "\n" +
                        "\t" + "\n" +
                        "\t" + "It's our time, time today" + "\n" +
                        "\t" + "The right time is here to stay" + "\n" +
                        "\t" + "Stay in my life, my life always" + "\n" +
                        "\t" + "Yellow is the color of sun rays" + "\n" +
                        "\t" + "\n" +
                        "\t" + "I hide myself, from no-one" + "\n" +
                        "\t" + "I know the time will surely come when" + "\n" +
                        "\t" + "You'll be in my life, my life always" + "\n" +
                        "\t" + "Yellow is the color of sun rays" + "\n" +
                        "\t" + "\n" +
                        "\t" + "Keep on moving (Keep on moving)" + "\n" +
                        "\t" + "Don't stop! (Keep on moving don't stop, no)" + "\n" +
                        "\t" + "Like the hands of time (Keep on moving)" + "\n" +
                        "\t" + "Click - clock, find your own way (Keep on moving)" + "\n" +
                        "\t" + "To stay (The time will come one day)" + "\n" +
                        "\t" + "\n" +
                        "\t" + "No - o - oh" + "\n" +
                        "\t" + "(Keep on moving) Hey - ey - ey" + "\n" +
                        "\t" + "(Keep on moving)" + "\n" +
                        "\t" + "(Keep on moving, don't stop, no)" + "\n" +
                        "\t" + "(Keep on moving) Oh!" + "\n" +
                        "\t" + "\n" +
                        "\t" + "I know the time, time today" + "\n" +
                        "\t" + "Walking alone in my own way" + "\n" +
                        "\t" + "Extremely cold and rainy day" + "\n" +
                        "\t" + "Friends and I have fun along the way" + "\n" +
                        "\t" + "Yes we do !" + "\n" +
                        "\t" + "\n" +
                        "\t" + "I hide myself from no one" + "\n" +
                        "\t" + "I know the time will really come when" + "\n" +
                        "\t" + "You'll be in my life, my life always" + "\n" +
                        "\t" + "Yellow is the color of sun rays" + "\n" +
                        "\t" + "\n" +
                        "\t" + "Keep on moving (Keep on moving)" + "\n" +
                        "\t" + "Don't stop (Keep on moving, don't stop, no)" + "\n" +
                        "\t" + "Like the hands of time (Keep on moving)" + "\n" +
                        "\t" + "Click - clock" + "\n" +
                        "\t" + "(Keep on moving) Find your own way to stay, yeah" + "\n" +
                        "\t" + "(The time will come one day) Yes!" + "\n" +
                        "\t" + "\n" +
                        "\t" + "Oh!" + "\n" +
                        "\t" + "(Keep on moving, don't stop, no)" + "\n" +
                        "\t" + "(Keep on moving) Ah - hey!" + "\n" +
                        "\t" + "(Keep on moving) Keep moving along, moving along" + "\n" +
                        "\t" + "(The time will come one day) Hey - yeah!" + "\n" +
                        "\t" + "\n" +
                        "\t" + "Keep movin', don't stop, no" + "\n" +
                        "\t" + "(Don't stop, like the hands of time) Hey-yeah!" + "\n" +
                        "\t" + "(Click - clock, find your own way to stay) Find your own way to stay" + "\n" +
                        "\t" + "(The time will come one day)" + "\n" +
                        "\t" + "(Why do people choose to live their lives this way ?) Oh - woh - oh" + "\n" +
                        "\t" + "Oh, why do they choose to live this way?" + "\n" +
                        "\t" + "\n" +
                        "\t" + "Yeah!" + "\n" +
                        "\t" + "Ah - hey!" + "\n" +
                        "\t" + "(Keep on moving) Keep on moving" + "\n" +
                        "\t" + "(Keep on moving, don't stop, no) No-woh-oh" + "\n" +
                        "\t" + "(Keep on moving) No!" + "\n" +
                        "\t" + "(Keep on moving) Keep on moving in my own way" + "\n" +
                        "\t" + "I know the time will come today" + "\n" +
                        "\t" + "(The time will come one day) Ah - hey!" + "\n" +
                        "\t" + "Yeah!" + "\n" +
                        "\t" + "(Keep on moving) Yeah - hey" + "\n" +
                        "\t" + "Keep on moving, don't stop, no) Don't stop" + "\n" +
                        "\t" + "Keep on moving) Ah - hey - ey" + "\n" +
                        "\t" + "(Keep on moving) Hey - yeah!" + "\n" +
                        "\t" + "(The time will come one day) The time will surely come one day!" + "\n" +
                        "\t" + "\n" +
                        "\t" + "Hey - yeah!" + "\n" +
                        "\t" + "(Keep on moving, don't stop, no) No-oh-oh!" + "\n" +
                        "\t" + "(Keep on moving) Oh - woh - oh" + "\n" +
                        "\t" + "(Keep on moving) Hey - yeah!" + "\n"
                        );
                    Console.Write("\nPress enter to continue: ");
                    Console.ReadLine();
                }
                

                // 0: Closing the program
                else if (answer == 0)
                {
                    Console.WriteLine("Thank you for your time. Goodbye!");
                    Console.ReadLine();
                    exit_program = true;
                }


                // 7: (NEW) Interactive grocery shopping list
                // show open list > choose one to remove > add the removed to checked list > show both lists
                else if (answer == 7)
                {
                    Console.Write("COMING SOON.. PRESS ENTER TO CONTINUE: ");
                    Console.ReadLine();
                }


                // Wrong input: program terminated
                else
                {
                    Console.WriteLine("\nThat is an invalid choice. This program will be terminated.");
                }


            }
            
        }
    }
}
