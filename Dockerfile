FROM mono:latest
COPY /Program.cs /Program.cs
RUN mcs Program.cs
RUN mono Program.exe

EXPOSE 6000
